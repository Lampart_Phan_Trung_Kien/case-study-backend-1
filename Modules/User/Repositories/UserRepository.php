<?php
namespace Modules\User\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
